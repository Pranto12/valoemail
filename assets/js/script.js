(function($){
	'use strict';
	
	$(document).ready(function() {  

		/*====TOP FIXED NAVBAR====*/

		$(document).on('scroll', function() {
			var scrollPos = $(this).scrollTop();

			if( scrollPos > 10 ) {
				$('.navbar-fixed-top').addClass('navbar-home');
			}

			else {
				$('.navbar-fixed-top').removeClass('navbar-home');
			}
		});

		/*====PORTFOLIO CAROUSEL====*/

		$('.feature-slider').owlCarousel({
			items: 1,
			loop: true,
			margin: 15,
			nav:false,
				
			responsive: {
				0: {
					items: 1,
					margin: 15,
				},
				768: {
					items: 1,
					margin: 15,
				},
				992: {
					items: 1,
					margin: 15,
				}
			}
		});
        
        $('.team-slider').owlCarousel({
			items: 3,
			loop: true,
			margin: 15,
			nav:false,
				
			responsive: {
				0: {
					items: 1,
					margin: 15,
				},
				768: {
					items: 2,
					margin: 15,
				},
				992: {
					items: 3,
					margin: 15,
				}
			}
		});
        
        

		/*====CLIENT SECTION====*/
		
		$('.sub-details.one').show().siblings().hide();
		$('.client.one').addClass('client_scale').show().siblings().hide();
		$('.selection-dot').on("click",function(){

			var self=$(this);
			self.addClass('active').siblings().removeClass('active');
			var model =self.attr('data-dot');
			
			$(".sub-details[data-details="+model+"]").show().siblings().hide();
			$(".client[data-client="+model+"]").addClass('client_scale').show().siblings().hide();
		});
		$('.client1').addClass('active').children().addClass('child'); //active 2 no. client.
			

		/*====CLIENT CAROUSEL FOR MOBILE====*/

		$('.client-mobile').owlCarousel({

			items: 1,
			margin: 10
		});

		/*====CONTACT FORM SECTION====*/

		$('.form-input').focus(function(){
			$(this).parent().addClass('is-focused has-label');
		});

		$('.form-input').each(function(){
			if($(this).val() != ''){
				$(this).parent().addClass('has-label');
			}
		});

		$('.form-input').blur(function(){
			var $parent = $(this).parent();
			if($(this).val() == ''){
				$parent.removeClass('has-label');
			}
			$parent.removeClass('is-focused');
		});

		//CONTACT FORM

		$('.contact-form form').on('submit', function(submit){
			submit.preventDefault();

			var result = $(this).find('.result p');
			var form   = this;

			$.post(
				$(this).attr('action'),
				$(this).serialize(),
				function(d){
					result.text(d);
					$(form).get(0).reset();
				}
			);
		});
	});

	$(window).load(function(){

		// Preloader
		$('.preloader').fadeOut('3000');
	});
	
})(jQuery);


/* Back To Top */
var timeOut;
function scrollToTop() {
    if (document.body.scrollTop!=0 || document.documentElement.scrollTop!=0){
        window.scrollBy(0,-50);
        timeOut=setTimeout('scrollToTop()', 10);
    }
    else clearTimeout(timeOut);
}